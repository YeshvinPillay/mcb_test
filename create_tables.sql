DROP TABLE SUPPLIER CASCADE CONSTRAINTS;
DROP TABLE order_headers_all CASCADE CONSTRAINTS;
DROP TABLE order_lines_all CASCADE CONSTRAINTS;
DROP TABLE Invoices_all CASCADE CONSTRAINTS;


CREATE TABLE SUPPLIER(
  supplier_id NUMBER,
  supplier_Name VARCHAR2(30),
  Supplier_contact_name VARCHAR2(50),
  address_line1 varchar2(240),
  address_line2 varchar2(240),
  address_line3 varchar2(240),
  address_line4 varchar2(240),
  Phone_number NUMBER,
  Mobile_Number NUMBER,
  Email varchar2(100),
  CONSTRAINT pk_supplier PRIMARY KEY (supplier_id)
);

CREATE Table order_headers_all (
  order_header_id NUMBER NOT NULL,
  order_date Date NOT NULL,
  supplier_id Number NOT NULL,
  ORDER_STATUS VARCHAR2(30) CHECK( order_status IN ('Open','Received','Closed','Cancelled') ),
  DESCRIPTION VARCHAR2(240),
  ORDER_Total_Amount NUMBER,
  CONSTRAINT order_headers_all_pk PRIMARY KEY (order_header_id),
  CONSTRAINT fk_order_headers_all_supplier FOREIGN KEY (supplier_id) REFERENCES supplier(supplier_id)
);


CREATE TABLE order_lines_all(
  order_header_id Number NOT NULL,
  order_line_id Number NOT NULL,
  DESCRIPTION VARCHAR2(240),
  order_line_status VARCHAR2(30) CHECK( order_line_status IN ('Open','Received','Closed','Cancelled') ),
  ORDER_line_amount NUMBER,
  CONSTRAINT pk_order_lines_all PRIMARY KEY (order_header_id,order_line_id),
  CONSTRAINT fk_order_lines_all_orders_headers_all FOREIGN KEY (order_header_id) REFERENCES order_headers_all(order_header_id)
);

CREATE TABLE INVOICES_ALL(
  INVOICE_ID NUMBER NOT NULL,
  order_header_id number NOT NULL,
  INVOICE_LINE_ID NUMBER NOT NULL,
  invoice_date date,
  invoice_status VARCHAR2(30) CHECK( invoice_status IN ('Pending','Paid')),
  Invoice_Hold_reason varchar2(240),
  Invoice_Amount number,
  description varchar2(240),
  CONSTRAINT pk_invoices_all PRIMARY KEY (Invoice_ID,INVOICE_LINE_ID),
  CONSTRAINT fk_invoice_orders_headers_all FOREIGN KEY (order_header_id) REFERENCES order_headers_all(order_header_id)
);

DROP SEQUENCE supplier_seq;

CREATE SEQUENCE supplier_seq START WITH 1;

CREATE OR REPLACE TRIGGER supp_bir
BEFORE INSERT ON supplier
FOR EACH ROW

BEGIN
  SELECT supplier_seq.NEXTVAL
  INTO   :new.supplier_id
  FROM   dual;
END;
/
