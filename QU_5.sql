SELECT
    ORDER_HEADER_ID AS ORDER_REFERENCE,
    TO_CHAR(ORDER_DATE,'MONTH DD,YYYY') AS ORDER_DATE,
    UPPER(SUPPLIER_NAME),
    TO_CHAR(ORDER_TOTAL_AMOUNT,'99,999,990.00') ORDER_TOTAL_AMOUNT,
    ORDER_STATUS
FROM
    (
        SELECT
            a.*,
            ROWNUM rnum
        FROM
            (
                SELECT
                    oha.*,supp.SUPPLIER_NAME
                FROM
                    order_headers_all oha,
                    supplier supp
                WHERE oha.supplier_id = supp.supplier_id
                ORDER BY
                    order_total_amount DESC
            ) a
    )
WHERE
    rnum = 3
;