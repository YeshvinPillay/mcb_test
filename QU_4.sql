SELECT
    oha.order_header_id,
    to_char(oha.order_date, 'MON-YY')                  order_period,
    initcap(supp.supplier_name),
    to_char(oha.order_total_amount, '99,999,990.00'),
    oha.order_status,
    inv.invoice_id,
    to_char(inv.invoice_total_amount, '99,999,990.00') invoice_total_amount,
    inv.action
FROM
    order_headers_all oha,
    supplier          supp,
    (
        SELECT
            invoice_id,
            order_header_id,
            SUM(invoice_amount) AS invoice_total_amount,
            decode(invoice_status, 'Paid', 'OK', 'Pending', 'To follow up',
                   'To verify') action
        FROM
            invoices_all
        GROUP BY
            invoice_id,
            order_header_id,
            invoice_status
    )                 inv
WHERE
        1 = 1
    AND oha.supplier_id = supp.supplier_id
    AND inv.order_header_id = oha.order_header_id
ORDER BY
    to_date(order_period, 'MM/YY') DESC,
    oha.order_header_id;
