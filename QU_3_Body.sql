
CREATE OR REPLACE PACKAGE BODY "MIGRATION_PROCESS_PCK"
AS


	FUNCTION F_GET_ADDRESS( V_ADDRESS VARCHAR2,
							V_LINE_NUMBER NUMBER)
	RETURN VARCHAR2
    IS
	V_NEW_ADD VARCHAR2(240);
	V_RETURN_ADD VARCHAR2(100);
	BEGIN
	
	V_RETURN_ADD := '-';
	SELECT REPLACE(V_ADDRESS,', Mauritius','') INTO V_NEW_ADD FROM DUAL;
	
	IF V_LINE_NUMBER = 3 THEN
	SELECT
	SUBSTR(
	V_NEW_ADD,
	INSTR(V_NEW_ADD,',',-1)+2,
	LENGTH(V_NEW_ADD))
	INTO
	V_RETURN_ADD
	FROM DUAL;
	
	ELSIF V_LINE_NUMBER = 2 THEN
	
	SELECT 
	REPLACE(
	V_NEW_ADD,
	SUBSTR(
	V_NEW_ADD,
	INSTR(V_NEW_ADD,',',-1),
	LENGTH(V_NEW_ADD)),
	'')
	INTO V_NEW_ADD
	FROM DUAL;
	
	SELECT
	SUBSTR(
	V_NEW_ADD,
	INSTR(V_NEW_ADD,',',-1)+2,
	LENGTH(V_NEW_ADD))
	INTO
	V_RETURN_ADD
	FROM DUAL;
	
	ELSIF V_LINE_NUMBER = 1 THEN
	
	SELECT 
	REPLACE(
	V_NEW_ADD,
	SUBSTR(
	V_NEW_ADD,
	INSTR(V_NEW_ADD,',',-1),
	LENGTH(V_NEW_ADD)),
	'')
	INTO V_NEW_ADD
	FROM DUAL;
	
	SELECT
	SUBSTR(
	V_NEW_ADD,
	INSTR(V_NEW_ADD,',',-1)+2,
	LENGTH(V_NEW_ADD))
	INTO
	V_RETURN_ADD
	FROM DUAL;
	
	SELECT REPLACE( V_NEW_ADD,V_RETURN_ADD,'')
	INTO V_RETURN_ADD
	FROM DUAl;
	
	--REMOVE TRAILING ,
	SELECT
	RPAD(V_RETURN_ADD,LENGTH(V_RETURN_ADD)-2)
	INTO V_RETURN_ADD
	FROM DUAL;
	
	END IF;
	RETURN V_RETURN_ADD;
	END;
	
	/* FUNCTION F_STRING_NUMBER_CLEANUP( V_STRING VARCHAR2)
	RETURN IS VARCHAR2
	V_RETURN_STR VARCHAR2(100);
	
	BEGIN
	SELECT
	REPLACE(REPLACE(REPLACE(REPLACE(regexp_replace(supp_contact_Number, '[[:space:]]*',''),'.'),'S','5'),'o','0'),'I',1) --REMOVE SPACES,.,S,o,I FROM STRINGS
	INTO
	V_RETURN_STR
	FROM 
	DUAL;
	END; */
	
	FUNCTION F_GET_FIXORMOB_NUMBER (V_STRING VARCHAR2,
									V_TYPE VARCHAR2)
	RETURN NUMBER
	IS
	V_RETURN_NUM NUMBER;
	BEGIN
	
	IF V_TYPE = 'MOB' THEN
	
		SELECT
		decode(nvl(length(substr(v_string, 0, instr(v_string, ',') - 1)), 0), 0, decode(length(v_string), 8, v_string, NULL), 8, substr(v_string,
		0, instr(v_string, ',') - 1), decode(length(substr(v_string, instr(v_string, ',') + 1, length(v_string))), 8, substr(v_string, instr(
		v_string, ',') + 1, length(v_string)), NULL))
	INTO v_return_num
	FROM
		dual;
		
		-- CHECK IF STRING CONTAINS , AT FIRST
		-- IF NOT THEN CHECK THE FIRST SET OF NUMBER IF IT IS 8 OR 7 FOR MOBILE OR PHONE
		-- IF CONTAINS STRING CHECK FIRST SET OF NUMBERS IF IT CONTAINS 7 OR 8
		--ELSE CHECK SECOND SET FOR 7 OR 8 FOR A MOBILE NUMBER
	ELSE
		SELECT
    decode(nvl(length(substr(v_string, 0, instr(v_string, ',') - 1)), 0), 0, decode(length(v_string), 7, v_string, NULL), 7, substr(v_string,
    0, instr(v_string, ',') - 1), decode(length(substr(v_string, instr(v_string, ',') + 1, length(v_string))), 7, substr(v_string, instr(
    v_string, ',') + 1, length(v_string)), NULL))
INTO v_return_num
FROM
    dual;
		
	END IF;
	RETURN V_RETURN_NUM;
	END;
	
PROCEDURE CREATE_SUPPLIERS
IS
CURSOR SUPPLIERS
IS
 SELECT 
 DISTINCT( supplier_name ),
    supp_contact_name,
    supp_address,
    replace(replace(replace(replace(regexp_replace(supp_contact_number, '[[:space:]]*', ''), '.'), 'S', '5'), 'o', '0'), 'I', 1) supp_contact_num, -- REMOVE CHARS/SPALCE/. IN PHONE NUMBER
    supp_email
FROM
    xxbcm_order_mgt;
	
REC_SUP SUPPLIER%ROWTYPE;
V_SUPPLIER_EXIST VARCHAR2(1);
V_SUPP_ID NUMBER;
BEGIN


FOR I IN SUPPLIERS
LOOP
BEGIN
select case 
            when exists (select 1 
                         from supplier 
                         where supplier_name = I.SUPPLIER_NAME) 
            then 'Y' 
            else 'N' 
        end as rec_exists
		into V_SUPPLIER_EXIST
from dual;
IF V_SUPPLIER_EXIST = 'N' THEN

REC_SUP.SUPPLIER_NAME := I.SUPPLIER_NAME;
REC_SUP.SUPPlier_CONTACT_NAME := I.SUPP_CONTACT_NAME;
REC_SUP.ADDRESS_LINE4:='MAURITIUS';
REC_SUP.ADDRESS_LINE3:=F_GET_ADDRESS(I.SUPP_ADDRESS,3);
REC_SUP.ADDRESS_LINE2:=F_GET_ADDRESS(I.SUPP_ADDRESS,2);
REC_SUP.ADDRESS_LINE1:=F_GET_ADDRESS(I.SUPP_ADDRESS,1);
REC_SUP.MOBILE_NUMBER := F_GET_FIXORMOB_NUMBER(I.supp_contact_num,'MOB');
REC_SUP.PHONE_NUMBER := F_GET_FIXORMOB_NUMBER(I.supp_contact_num,'FIX');
REC_SUP.EMAIL := I.SUPP_EMAIL;
INSERT INTO SUPPLIER VALUES REC_SUP;
COMMIT;
END IF;
EXCEPTION
	WHEN OTHERS THEN
	/*PUT ERROR */
	DBMS_OUTPUT.PUT_LINE('SUPPLIER WITH NAME:'|| I.SUPPLIER_NAME ||' COULD NOT BE CREATED');
END;
END LOOP;	
END;

PROCEDURE CREATE_ORDERS	
IS
CURSOR ORDERS
IS
SELECT order_ref,
       order_date,
       ALPHA.order_num,
       order_tot,
       order_description,
       order_status,
       SUM(order_line_amount) ORDER_LINE_AMOUNT,
       supplier_name
FROM   (SELECT DISTINCT order_ref,
                        Nvl(Rpad(order_ref, Instr(order_ref, '-') - 1),
                        order_ref)
                               ORDER_NUM,
                        Decode(( Instr(order_date, '-', -1) -
                                 Instr(order_date, '-') ),
                        3, To_date(
                        order_date, 'DD/MM/YYYY'),
                        To_date(
                        order_date, 'DD/MON/YYYY'))
                               AS ORDER_DATE,
                        To_number(( Replace(Replace(Replace(Replace(Replace(
                                            Regexp_replace(order_total_amount,
                                            '[[:space:]]*', ''
                                                      ),
                                                                '.'),
                                                                'S', '5'
                                            ), 'o',
                                              '0')
                                                        , 'I', 1
                                            ), ',', '') ))
                               AS ORDER_TOTAL_AMOUNT,
                        order_description,
                        order_status,
                        supplier_name,
                        To_number(( Replace(Replace(Replace(Replace(Replace(
                                            Regexp_replace(order_line_amount,
                                                        '[[:space:]]*'
                                                                , ''),
                                                                '.'),
                                            'S'
                                            , '5'), 'o', '0'), 'I', 1), ',', '')
                                  )) AS
                        ORDER_LINE_AMOUNT
        FROM   xxbcm_order_mgt) ALPHA,
       (SELECT order_num,
               SUM(order_total) ORDER_TOT
        FROM  (SELECT Nvl(Rpad(order_ref, Instr(order_ref, '-') - 1), order_ref)
                             order_num,
                      To_number(( Replace(Replace(Replace(Replace(Replace(
                                          Regexp_replace(order_line_amount,
                                                      '[[:space:]]*'
                                                              , ''),
                                                              '.'),
                                          'S'
                                          , '5'), 'o', '0'), 'I', 1), ',', '')
                                )) AS
                      ORDER_TOTAL
               FROM   xxbcm_order_mgt
               WHERE  order_line_amount IS NOT NULL)
        GROUP  BY order_num) BETA -- THIS SUB-TABLE DISPLAYS THE PO_TOTAL_AMOUNT BY PO
WHERE  ALPHA.order_num = BETA.order_num
GROUP  BY order_ref,
          order_date,
          order_tot,
          ALPHA.order_num,
          order_description,
          order_status,
          supplier_name
ORDER  BY order_ref; 

REC_OHA ORDER_HEADERS_ALL%ROWTYPE;
REC_OLA ORDER_LINES_ALL%ROWTYPE;
V_ORDER_HEADER_ID NUMBER;
V_SUPP_ID NUMBER;
V_LINE_ID NUMBER;
BEGIN
FOR I IN ORDERS
LOOP
BEGIN
V_ORDER_HEADER_ID:=TO_NUMBER(REPLACE(I.ORDER_NUM,'PO',''));

IF I.ORDER_LINE_AMOUNT IS NULL THEN 
REC_OHA.order_header_id := V_ORDER_HEADER_ID;
REC_OHA.ORDER_DATE := TO_DATE(I.ORDER_DATE,'DD-MON-YYYY');
REC_OHA.DESCRIPTION:= I.ORDER_DESCRIPTION;
REC_OHA.ORDER_TOTAL_AMOUNT := I.ORDER_TOT;
REC_OHA.ORDER_STATUS:=I.ORDER_STATUS;

BEGIN
	SELECT
	SUPPLIER_ID
	INTO
	V_SUPP_ID
	FROM
	SUPPLIER
	WHERE SUPPLIER_NAME = I.SUPPLIER_NAME;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
	 V_SUPP_ID:= NULL;


END;
REC_OHA.SUPPLIER_ID := V_SUPP_ID;
INSERT INTO ORDER_HEADERS_ALL VALUES REC_OHA;

ELSE
REC_OLA.ORDER_HEADER_ID :=V_ORDER_HEADER_ID;
BEGIN
SELECT
MAX(ORDER_LINE_ID)
INTO V_LINE_ID
FROM ORDER_LINES_ALL
WHERE ORDER_HEADER_ID = V_ORDER_HEADER_ID;
EXCEPTION
	WHEN OTHERS THEN
	 V_LINE_ID:= 0;
END;

V_LINE_ID:=V_LINE_ID+1;
REC_OLA.ORDER_LINE_ID :=NVL(V_LINE_ID,1);
REC_OLA.DESCRIPTION :=I.ORDER_DESCRIPTION;
REC_OLA.ORDER_LINE_STATUS := I.ORDER_STATUS;
REC_OLA.ORDER_LINE_AMOUNT := TO_NUMBER(I.ORDER_LINE_AMOUNT);

INSERT INTO ORDER_LINES_ALL VALUES REC_OLA;
END IF;
END;
END LOOP;
END; 

PROCEDURE CREATE_INVOICES
IS
CURSOR INVOICES
IS
SELECT
    invoice_reference,
    invoice_date,
    invoice_status,
    invoice_hold_reason,
    SUM(invoice_amt) invoice_amount,
    invoice_description
FROM
    (
        SELECT
            invoice_reference,
            decode((instr(invoice_date, '-', - 1) - instr(invoice_date, '-')), 3, to_date(invoice_date, 'DD/MM/YYYY'), to_date(invoice_date,'DD/MON/YYYY'))   AS    invoice_date,
            invoice_status,
            invoice_hold_reason,
            to_number((replace(replace(replace(replace(replace(regexp_replace(invoice_amount, '[[:space:]]*', ''), '.'), 'S', '5'), 'o','0'), 'I', 1), ',', ''))) AS invoice_amt,
            invoice_description
        FROM
            xxbcm_order_mgt
    ) invoices
WHERE
    invoice_reference IS NOT NULL
GROUP BY
    invoice_reference,
    invoice_date,
    invoice_status,
    invoice_hold_reason,
    invoice_description
ORDER BY
    invoice_reference;
	
REC_INV INVOICES_ALL%ROWTYPE;
V_INVOICE_REF VARCHAR2(35);
V_INVOICE_ID NUMBER;
V_LINE_ID NUMBER;
BEGIN
FOR I IN INVOICES
LOOP
BEGIN
V_INVOICE_REF:=REPLACE(I.invoice_reference,'INV_PO','');
V_INVOICE_ID :=  NVL(RPAD(V_INVOICE_REF,INSTR(V_INVOICE_REF,'.')-1),V_INVOICE_REF);
REC_INV.INVOICE_ID := V_INVOICE_ID;
REC_INV.ORDER_HEADER_ID := V_INVOICE_ID;
BEGIN
SELECT
MAX(INVOICE_LINE_ID)
INTO V_LINE_ID
FROM INVOICES_ALL
WHERE INVOICE_ID = V_INVOICE_ID;
EXCEPTION
	WHEN OTHERS THEN
	 V_LINE_ID:= 0;
END;
V_LINE_ID:=V_LINE_ID+1;
REC_INV.INVOICE_LINE_ID :=NVL(V_LINE_ID,1);
REC_INV.INVOICE_DATE:=I.INVOICE_DATE;
REC_INV.INVOICE_STATUS:=I.INVOICE_STATUS;
REC_INV.INVOICE_HOLD_REASON:=I.INVOICE_HOLD_REASON;
REC_INV.INVOICE_AMOUNT:=I.INVOICE_AMOUNT;
REC_INV.DESCRIPTION:=I.INVOICE_DESCRIPTION;

INSERT INTO INVOICES_ALL VALUES REC_INV;
END;
END LOOP;

END;

PROCEDURE MIGRATION

IS
  


-----VARIABLE DECLARATION -----


BEGIN
 CREATE_SUPPLIERS;
 CREATE_ORDERS;
 CREATE_INVOICES;
 COMMIT;
 --CREATE SEQUENCE PO_LINE_seq START WITH 1;
--SELECT REPLACE(tb.ORDER_REF,'PO'), tb.* FROM xxbcm_order_mgt tb order by 1 ;

end;
END;